/* ----------------------------------------------------------------
|  Libs
*/

var gulp = require('gulp'),
	del = require('del'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	sourcemaps = require('gulp-sourcemaps'),
	svgmin = require('gulp-svgmin'),
	rename = require('gulp-rename'),
	cache = require('gulp-cache'),
	install = require('gulp-install'),
  replace = require('gulp-replace')

/* ----------------------------------------------------------------
|  Filepaths
*/

	// Development files
	var dev = {
		config: ['bower.json','package.json'],
		img: ['img/**/*.jpg','img/**/*.jpeg','img/**/*.gif','img/**/*.png'],
		svg: ['img/**/*.svg'],
		js: ['js/*.js'],
		data: ['data/*.json'],
		libs: [
      'vendor/modernizr/modernizr.js',
			'vendor/jquery/dist/jquery.js',
			'vendor/angular/angular.js',
			'vendor/angular-ui/build/angular-ui.js',
      'vendor/angular-ui-router/release/angular-ui-router.js',
			'vendor/angular-animate/angular-animate.js',
			'vendor/angular-foundation/mm-foundation.js'
		],
		sass: ['scss/*.scss'],
		fonts: ['fonts/*'],
    html: ['html/*']
	};

	// Compiled build assets
	var build = {
		img: 'public/img',
		js: 'public/js',
		css: 'public/css',
    html: 'public/html'
	};

	gulp.task('clean', function(cb) {
		del(['public/**/*'], cb)
	});

	gulp.task('install', function () {
		gulp.src(dev.config)
		.pipe(install());
	});

/* ----------------------------------------------------------------
|  SASS/SCSS
*/

	gulp.task('sass', function () {
		return gulp.src(dev.sass)
		// .pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths : ['vendor/foundation/scss'],
			errLogToConsole: true
		}))
		.pipe(autoprefixer("last 1 version", "> 1%", "ie 8", "ie 7"))
		// .pipe(sourcemaps.write())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(build.css));
	});

	gulp.task('fonts', function() {
		return gulp.src(dev.fonts)
		.pipe(gulp.dest(build.css));
	});

  gulp.task('html', function() {
    return gulp.src(dev.html)
    .pipe(gulp.dest(build.html));
  });

/* ----------------------------------------------------------------
|  Javascript
*/

	// Vendor JS
	gulp.task('libs', function() {
		return gulp.src(dev.libs)
		// .pipe(sourcemaps.init())
		.pipe(concat('libs.min.js'))
		.pipe(uglify({mangle: false}))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest(build.js));
	});

	// Project JS
	gulp.task('js', function() {
		return gulp.src(dev.js)
		// .pipe(sourcemaps.init())
		.pipe(concat('app.min.js'))
		.pipe(uglify({mangle: false}))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest(build.js));
	});

	// Datafiles
	gulp.task('data', function() {
		return gulp.src(dev.data)
		// .pipe(sourcemaps.init())
		// .pipe(uglify({mangle: false}))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest(build.js));
	});

/* ----------------------------------------------------------------
|  Images
*/

	gulp.task('img', function() {
		return gulp.src(dev.img)
		.pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
		.pipe(gulp.dest(build.img));
	});

	gulp.task('svg', function() {
		return gulp.src(dev.svg)
		// .pipe(svgmin())
    // .pipe(replace(/_[0-9]+_/g, ''))
		.pipe(gulp.dest(build.img));
	});

/* ----------------------------------------------------------------
|  Task config
*/

	gulp.task('watch', function() {
		gulp.watch(dev.img,  ['img']);
		gulp.watch(dev.svg,  ['svg']);
		gulp.watch(dev.js,   ['js']);
		gulp.watch(dev.libs,  ['libs']);
		gulp.watch(dev.sass, ['sass']);
		gulp.watch(dev.fonts,  ['fonts']);
		gulp.watch(dev.data,  ['data']);
	});

	gulp.task('initialise', ['install', 'build']);
	gulp.task('build', ['img', 'svg', 'sass', 'fonts', 'js', 'libs', 'data', 'html']);
	gulp.task('default', ['build','watch']);
