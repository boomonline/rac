app.controller('appController', function($rootScope, $scope, $state, JSONService, $interval) {
    $scope.$state = $state;
    $scope.scrollDown = false; // Scrolling down on command

    $scope.states = ['home','planning','cold','rain','fog','snow','night'];
    var l = $scope.states.length;

    // Next / Prev nav
    // nav.(curr|prev|next)();
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      var curr = toState.name;
      var i = $scope.states.indexOf(curr);
      var prev = $scope.states[i-1] ? $scope.states[i-1] : 'start';
      var next = i+1 < l ? $scope.states[i+1] : 'start';
      $scope.nav = {
        curr: function() { return curr; },
        prev: function() { $state.go(prev); },
        next: function() { $state.go(next); }
      };
    })

    JSONService.fetch().then(function(data) {
      $scope.matrix = data;
    });

    $scope.scroll = {
      toggle: function() {
        $scope.scrollDown = !$scope.scrollDown;
        angular.element('.rac-tip').remove();
      },
      get: function() {
        return $scope.scrollDown;
      }
    }

    $scope.removePopups = function() {
      var tip = $('.rac-tip');
      var delay = 750 // ms before hiding is possible.

      if(tip.length > 0 && (new Date - tip.data('initialised')) > delay) {
        angular.element('.rac-tip').remove();
      }
    }

    $scope.yayaya = function($event) {
      console.log($event);
    }

    // $scope.hoversafe = [];

    // $scope.hoverOver = function($event) {
    //   console.log($scope.scrolldown)
    //   $scope.scrolldown = true;
    //   console.log($event);
    // }
    // $scope.hoverOut = function () {
    //   $scope.scrolldown = false
    // }
});

app.controller('packingController', function($scope, $state, JSONService) {
  JSONService.fetch().then(function(data) {
    $scope.tip = data.states['home'].tips['packing'];
  });
});
