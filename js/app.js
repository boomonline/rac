/* -- APP.JS -- */

'use strict';

// Angular
var app = angular.module("app", ['ui.router','mm.foundation', 'ngAnimate']);
app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('start', {
      url: "/",
      templateUrl: "public/html/start.htm"
    })
    .state('home', {
      url: "/home",
      templateUrl: "public/html/home.htm"
    })
    .state('packing', {
      url: "/packing",
      templateUrl: 'public/html/packing.htm',
      controller: 'packingController'
    })
    .state('planning', {
      url: "/planning",
      templateUrl: "public/html/planning.htm"
    })
    .state('cold', {
      url: "/cold",
      templateUrl: "public/html/cold.htm"
    })
    .state('rain', {
      url: "/rain",
      templateUrl: "public/html/rain.htm"
    })
    .state('fog', {
      url: "/fog",
      templateUrl: "public/html/fog.htm"
    })
    .state('snow', {
      url: "/snow",
      templateUrl: "public/html/snow.htm"
    })
    .state('night', {
      url: "/night",
      templateUrl: "public/html/night.htm"
    })
});

var decentBrowser = false;

app.run(function($rootScope, $state, TransitionService, JSONService) {
  $rootScope.Modernizr = Modernizr;
  $rootScope.browser = function() {
      var ua= navigator.userAgent, tem,
      M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
      if(/trident/i.test(M[1])){
          tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
          return 'IE '+(tem[1] || '');
      }
      if(M[1]=== 'Chrome'){
          tem= ua.match(/\bOPR\/(\d+)/)
          if(tem!= null) return 'Opera '+tem[1];
      }
      M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
      if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
      return M;
  };

  var browser = $rootScope.browser();

  if(browser[0] == 'Chrome' || browser[0] == 'Safari') {
    decentBrowser = $rootScope.decentBrowser = true;
    $("html").addClass('decent');
  }
  else {
    decentBrowser = $rootScope.decentBrowser = false;
    $("html").addClass('indecent');
  }

  if(browser[0] != "Firefox") {
    $("html").removeClass('moz');
    $("html").addClass('no-moz');
  }

  if(
      browser[0] == "MSIE"
  ||  browser[0] == "I E"
  ||  browser == "I E"
  ||  browser == "IE"
  || (browser[0] == "I" && browser[1] == "E")
  ||  browser[1] == "IE"
  ||  browser[1] == "I E"
  ||  browser[1] == "11"
  ) {
    $("html").addClass('ie9');
    $("html").removeClass('no-ie9');
    if(browser[1] == 11) {
    } else {
    }
  } else {
    $("html").addClass('no-ie9');
    $("html").removeClass('ie9');
  }

  var container = angular.element("#ui-view-container");

  JSONService.fetch().then(function(data) {
    $state.go("start");

    $rootScope.states = data.states;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

        TransitionService.set($rootScope.states[fromState.name].classes,$rootScope.states[toState.name].classes);

        // console.log(fromState.name+" disappears like "+TransitionService.get().leave)
        // console.log(toState.name+" appears like "+TransitionService.get().enter)

        $rootScope.fromEl = angular.element("."+fromState.name).addClass(TransitionService.get().leave)
        $rootScope.toEl = angular.element("."+toState.name).addClass(TransitionService.get().enter)
        container.removeClass().addClass(TransitionService.get().enter)
    })
  });
})
