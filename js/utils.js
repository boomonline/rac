function check(arr,str) {
  return (arr.indexOf(str) > -1);
}

function offset(el) {
    var convertPoint = window.webkitConvertPointFromNodeToPage;
    if ('getBoundingClientRect' in el) {
        var
            boundingRect = el.getBoundingClientRect(),
            body = document.body || document.getElementsByTagName("body")[0],
            clientTop = document.documentElement.clientTop || body.clientTop || 0,
            clientLeft = document.documentElement.clientLeft || body.clientLeft || 0,
            scrollTop = (window.pageYOffset || document.documentElement.scrollTop || body.scrollTop),
            scrollLeft = (window.pageXOffset || document.documentElement.scrollLeft || body.scrollLeft);
        return {
            top: boundingRect.top + scrollTop - clientTop,
            left: boundingRect.left + scrollLeft - clientLeft
        }
    } else if (convertPoint) {
        var
            zeroPoint = new WebKitPoint(0, 0),
            point = convertPoint(el, zeroPoint),
            scale = convertPoint(document.getElementById('scalingEl'), zeroPoint);
        return {
            top: Math.round(point.y * -200/scale.y),
            left: Math.round(point.x * -200/scale.x)
        }
    }
}
