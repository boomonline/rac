app.factory('JSONService', function($q, $timeout, $http) {
    var data;
    var matrix = {
        fetch: function(callback) {

            var deferred = $q.defer();

            $timeout(function() {
                $http.get('public/js/data.json').success(function(json) {
                    deferred.resolve(json);
                    data = json;
                });
            }, 30);

            return deferred.promise;
        },
        get: function(callback) {
            return data;
        }
    };

    return matrix;
});

app.factory('TransitionService', [function() {
    var transitions = {};

    return {
        set: function(leave, enter) {

              var leaveAnim='fadeOut';
              var enterAnim='fadeIn';

              if (check(leave,'home')) {
                if (check(enter,'ui-scene')) {
                  enterAnim = 'smallbig'
                } else if (check(enter,'parked')) {
                    leaveAnim='panOutLeft';
                    enterAnim = 'fromStatic panInRight rollInLeft'
                }
              } else if (check(leave,'parked')) {
                if(check(enter,'home')) {
                  leaveAnim = 'panOutRight';
                  enterAnim = 'panInLeft';
                } else if (check(enter,'exterior') && check(enter,'singleton')) {
                  leaveAnim = 'driveOutRight panOutLeft'
                  enterAnim = 'driveInLeft panInRight'
                } else if (check(enter,'singleton')) {
                  leaveAnim = 'driveOutRight'
                  enterAnim = 'driveInLeft'
                } else {
                  leaveAnim='fadeOut';
                  enterAnim='fadeIn';
                }
              } else if(check(leave,'exterior') && check(leave,'singleton')) {
                  leaveAnim='driveOutRight panOutLeft'

                  if(check(enter,'exterior')) {
                    enterAnim='driveInLeft panInRight';
                  }
              } else if(check(leave,'cockpit')) {
                leaveAnim='fadeOut';

                if(check(enter,'exterior')) {
                  enterAnim='driveInLeft';
                }
              }

            transitions.enter = enterAnim;
            transitions.leave = leaveAnim;

            // console.log("I figure...")
            // console.log(transitions);
        },
        get: function() {
            return transitions;
        }
    };
}]);
