// popup data-popup-id="highgear"
app.directive('popup', function(JSONService, $state) {
    return {
      link : function($scope, $element, $attrs) {
        var tipClass = 'rac-tip';

        var popup = angular.element(
          "<div class='"+tipClass+"'>"+
          " <i class='rac-tip__closebtn fa fa-times-circle'></i>"+
          " <h3 class='rac-tip__title'>ThisThing</h3>"+
          " <p class='rac-tip__content'>Popup</p>"+
          " <div class='rac-tip__links'></div>"+
          "</div>"
        );

        $element.bind('click', function($event) {
            $event.stopPropagation()
            // var pos = $element[0].getBoundingClientRect();
            var pos = offset($element[0]);

            angular.element("."+tipClass).remove();

            var id = $element.context.attributes['data-popup-id'].value;

            // Load data
            var matrix = JSONService.get();
            var tip = matrix.states[$state.current.name].tips[id];

            // Display data
            popup.children(".rac-tip__title").text(tip.title);
            popup.children(".rac-tip__content").text(tip.text);
            if(typeof tip.scroll != 'undefined') popup.addClass('scroll');

            if(tip.link != null) {
              popup.children(".rac-tip__links").empty();
              angular.forEach(tip.link, function(tipLink) {
                popup.children(".rac-tip__links")
                  .append('<a href="'+tipLink.url+'"><b>'+tipLink.text+'</b> @ RAC Shop <i class="fa fa-arrow-right"></i></a>');
              })
            }

            if(tip.youtube != null) {
              popup.addClass("rac-tip--youtube");
              popup.children(".rac-tip__content").append('<iframe src="http://www.youtube.com/embed/'+tip.youtube+'" frameborder="0" allowfullscreen></iframe>');
            } else {
              var thisSlide = $(".state")[0];
              var canvas = {
                h: thisSlide.clientHeight,
                w: thisSlide.clientWidth
              }

              var dirClass = "arrow";
              if(pos.top < (2 * (canvas.h/3))) {
                dirClass += 'Top'; popup.css("top", pos.top-80)
              } else {
                dirClass += 'Bottom'; popup.css("top", pos.top-80)
              }
              if(pos.left < (canvas.w/2)) {
                dirClass += 'Left'; popup.css("left", pos.left)
              } else {
                dirClass += 'Right'; popup.css("left", pos.left)
              }

              popup.addClass(dirClass);
            }

            popup
              .data('initialised',new Date)
              .appendTo(".state")

            angular.element('.state').bind('click', function() {
              $scope.removePopups();
            });
        });
      }
    };
});

app.directive('weather', function() {
    return {
        restrict: 'E',
        controller: function($scope, $attrs) {
            $scope.clouds = new Array(3);
            $scope.streams = new Array(16);
            $scope.drops = new Array(6);
            $scope.weathertype = $attrs.weathertype;
        },
        replace: true,
        template: "<div>"+
                        "<div class='weather {{weathertype}}'>"+
                            "<div class='cloud' ng-repeat='i in clouds track by $index'>"+
                                "<div class='stream' ng-repeat='i in streams track by $index'>"+
                                    "<div class='drop' ng-repeat='i in drops track by $index'></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                    "</div>",
    }
})
