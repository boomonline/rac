<?php require("config.php");

$app = new App();
$app->HTMLINJECT = "ng-app='app' id='ng-app' xmlns:ng='http://angularjs.org'";
$app->HTML_CLASSES = "ie9 moz";
$app->BODYINJECT = "ng-controller='appController'";
// $app->PAGETITLE;

$app->document_head(); ?>

<div id="social-panel">
  <div>
    <!-- Go to www.addthis.com/dashboard to generate a new set of sharing buttons -->
    <a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=http%3A%2F%2Fwww.addthis.com&pubid=ra-535fa9d114a6f162&ct=1&title=AddThis%20-%20Get%20likes%2C%20get%20shares%2C%20get%20followers&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>
    <a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=http%3A%2F%2Fwww.addthis.com&pubid=ra-535fa9d114a6f162&ct=1&title=AddThis%20-%20Get%20likes%2C%20get%20shares%2C%20get%20followers&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>
    <a href="https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=http%3A%2F%2Fwww.addthis.com&pubid=ra-535fa9d114a6f162&ct=1&title=AddThis%20-%20Get%20likes%2C%20get%20shares%2C%20get%20followers&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/google_plusone_share.png" border="0" alt="Google+"/></a>
    <a href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=http%3A%2F%2Fwww.addthis.com&pubid=ra-535fa9d114a6f162&ct=1&title=AddThis%20-%20Get%20likes%2C%20get%20shares%2C%20get%20followers&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/linkedin.png" border="0" alt="LinkedIn"/></a>
    <a href="https://www.addthis.com/bookmark.php?source=tbx32nj-1.0&v=300&url=http%3A%2F%2Fwww.addthis.com&pubid=ra-535fa9d114a6f162&ct=1&title=AddThis%20-%20Get%20likes%2C%20get%20shares%2C%20get%20followers&pco=tbxnj-1.0" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/addthis.png" border="0" alt="Addthis"/></a>
  </div>
</div>

<div id="wrapper">
    <header id="document-head">
        <nav class="row" id="document-nav">
            <a href='#' class="state-link" id="nav-start"
            ng-click="selected = 'start'"
            ui-sref="start">
                <div class='icon'>
                    <img src='<?=$app->IMGURL?>/icons/logo_icon.png'>
                </div>
            </a>
            <a href='#' ng-repeat="state in states track by $index"
              class="state-link state-link--std show-for-medium-up" id="nav-{{state}}"
              ng-click="$parent.selected = state"
              ng-class="$parent.selected == state ? 'selected' : 'deselected'"
              ui-sref="{{state}}">
                <div class='icon'>
                    <img ng-src='<?=$app->IMGURL?>/icons/nav_{{state}}.svg' />
                </div>
            </a>
            <a ng-click='nav.prev()' class="state-link-dirs">
              <i class="fa fa-arrow-left" ng-show="nav.curr() != 'start'"></i>
              <i class="fa fa-arrow-left" style="opacity: 0;" ng-show="nav.curr() == 'start'"></i>
            </a>
            <a ng-click='nav.next()' class="state-link-dirs end">
              <i class="fa fa-arrow-right"></i>
            </a>
        </nav>
    </header>

    <main class="row">
        <div class="small-12" id="ui-view-container" slide-state>
            <div ui-view class="{{ $state.current.name }} {{ matrix.states[$state.current].classes }} state"></div>
        </div>
    </main>
</div>

<? $app->document_foot() ?>
